<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ECU 911 - Adminstrador</title>
    <link rel="shortcut icon" href="img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/d240db18f0.js" crossorigin="anonymous"></script>
</head>

<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
            <div class="sidebar-brand">
                <a href="#">ECU 911</a>
                <div id="close-sidebar">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="sidebar-header">
                <div class="user-pic">
                    <img class="img-responsive img-rounded"
                        src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
                        alt="User picture">
                </div>
                <div class="user-info">
                    <span class="user-name">Erik
                        <strong>Daniel</strong>
                    </span>
                    <span class="user-role">Administrator</span>
                    <span class="user-status">
                        <i class="fa fa-circle"></i>
                        <span>Online</span>
                    </span>
                </div>
            </div>
            <!-- sidebar-header  -->
            <div class="sidebar-menu">
                <ul>
                    <li class="sidebar-dropdown">
                        <a href="#">
                            <i class="fas fa-newspaper"></i>
                            <span>Incidentes</span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <a href="http://localhost:8000/home">Listar Incidentes

                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="#">
                            <i class="fa fa-user-friends"></i>
                            <span>Recursos</span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <a href="http://localhost:8000/recursos">Listar Recursos

                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="#">
                            <i class="far fa-edit"></i>
                            <span>Updates</span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <a href="http://localhost:8000/update">Actualizar turnos</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="#">
                            <i class="fas fa-mask"></i>
                            <span>Configurar Macs</span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <a href="http://localhost:8000/macs">Listar MACs</a>
                                </li>
                                <li>
                                    <a href="http://localhost:8000/macs-users">Listar Usuarios conectados</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- sidebar-content  -->
        <div class="sidebar-footer">
            <a href="#">
                <i class="fas fa-door-open"></i> Cerrar Sesión
            </a>
        </div>
    </nav>
    </nav>

    <main class="page-content">
        <div class="container-fluid">
            <h2>Update</h2>
            <hr>
            <div class="row">
                <!-- AGREGEN TODO EL CODIGO QUE NECESITEN PARA SU VISTA-->

                <h5>En esta sección agreguen todo el codigo que necesiten para su vista</h5>
                <h5>Pueden hacer uso de las class de bootstrap v3.6 / v3.7 ya esta cargado</h5>
                <h5>Si van agregar una imagen agregan en la ruta /public/img/</h5>
                <h5>Si van agregar algun icono pueden usar los iconos de https://fontawesome.com/ , ya esta cargado al
                    igual que bootstrap solo llamen al icono</h5>
                <h5>Si van agregar su propio estilo, animaciones crean un arhivo nuevo en la ruta /public/css/ y lo
                    llaman en la cabezera de la plantilla</h5>
                <h5><b>Espero sean responsables y me envien hasta el lunes en la noche para unir que el martes toca
                        presentar
                        avance >:v</b></h5>


            </div>
        </div>
    </main>
</div>

<script>
$(".sidebar-dropdown > a").click(function() {
    $(".sidebar-submenu").slideUp(200);
    if (
        $(this)
        .parent()
        .hasClass("active")
    ) {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
            .parent()
            .removeClass("active");
    } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
        $(this)
            .parent()
            .addClass("active");
    }
});

$("#close-sidebar").click(function() {
    $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
    $(".page-wrapper").addClass("toggled");
});
</script>

</html>