<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ECU 911 - Login</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/login.css" />
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
</head>

<body class="fondo">
    <div class="col-lg-12">
        <div class="col-lg-2"></div>
        <div class="centrar-logo col-lg-4">
            <div class="image-logo">
                <img src="img/logo.png">
            </div>
        </div>

        <div class="col-lg-1">

        </div>

        <div class="centrar col-lg-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="text-center"><b>Bienvenido, Erik-Daniel</b></h4>
                    <br />
                    <div class="image-user center">
                        <img src="img/user.png">
                    </div>
                    <br>
                    <p>
                        <label>Contraseña:</label>
                        <input class="form-control" type="password" name="password" required>
                    </p>
                    <p class="text-center">
                        <a class="btn btn-danger btn-block" href="http://localhost:8000/home">Continuar</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</body>

</html>