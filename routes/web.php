<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login/login');
});

Route::get('/home', function () {
    return view('administrador/principal');
});

Route::get('/recursos', function () {
    return view('administrador/recursos');
});

Route::get('/update', function () {
    return view('administrador/update');
});

Route::get('/macs', function () {
    return view('administrador/macs');
});

Route::get('/macs-users', function () {
    return view('administrador/macs-users');
});